'''
Created on 07/10/2020

@author: cacic2020
'''
import random
class Naipe(object):
    def __init__(self, *args,**kargs):
        self.palo=kargs["palo"]
        self.numero=kargs["numero"]
    def getPalo(self):
        return self.palo
    def getNumero(self):
        return self.numero
class Baraja():
    def __init__(self):
        self.__baraja=list()
        palos=["oro","espada","basto","copa"]       
        for x in palos:
            for y in range(1,13):
                self.__baraja.append(Naipe(numero=y,palo=x))
                
    def getSuperior(self):
        print(len(self.__baraja))
        carta=self.__baraja[len(self.__baraja)-1]
        self.__baraja.pop(len(self.__baraja)-1)
        return "La Carta Superior es",carta.__getattribute__("numero")," de ",carta.__getattribute__("palo")
    def getInferior(self):
        carta=self.__baraja[0]
        self.__baraja.pop(0)
        return "La Carta Inferior es",carta.__getattribute__("numero")," de ",carta.__getattribute__("palo")
    def getCartas(self):
        for x in range(0,len(self.__baraja)):
            cartnumero=self.__baraja[x].__getattribute__("numero")
            cartpalo=self.__baraja[x].__getattribute__("palo")
            print("(",cartnumero,",",cartpalo,")")
    def mezclar(self):
        if(len(self.__baraja)==48):
            random.shuffle(self.__baraja)
            print("Termine de mezclar")
        else:
            raise Exception("La baraja no se puede mezclar deben haber 48 cartas y hay ",len(self.__baraja))
    
    def findCarta(self,palo,numero):
        for x in range(0,len(self.__baraja)):
            cartnumero=self.__baraja[x].__getattribute__("numero")
            cartpalo=self.__baraja[x].__getattribute__("palo")
            if(cartnumero==numero and cartpalo==palo):
                return True
        
        return False
    
    def setCarta(self,elpalo,elnumero):
        if(self.findCarta(elpalo,elnumero)):
            raise Exception("No se puede agregar esta carta")
        else:
            self.__baraja.append(Naipe(numero=elnumero,palo=elpalo))
            print("Listo!.Tenemos ",len(self.__baraja)," cartas en la baraja")
                     