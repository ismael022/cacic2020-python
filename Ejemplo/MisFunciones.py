'''
Created on 05/10/2020

@author: cacic2020
'''
import sys#Ej3
import matplotlib.pyplot as plt#Ej4
import struct#Ej4

def saludar():
    n=input("Ingrese su N0mbre: ")
    saludo = "Bienvenido a CACIC 2020 - JUJUY ",n
    print(saludo)
    return "Proceso terminado"

def recibir():
        print("Ingrese argumentos en Run/Configuration/Arguments o bien por Consola")
        i=len(sys.argv)
        if (i<5):
            for v in sys.argv:
                print(v)
                    
        else:
                        print("Ha ocurrido un error.Solo se admite hasta 5 argumentos")
              
        return "Proceso terminado con exito"

def validar():
    resultado=0
    contador=0
    print("Ingrese argumentos en Run/Configuration/Arguments o bien por Consola")
    for v in sys.argv[1:]:
        print(v)
        for j in v:
            if(j!="-"):
                numero=int(j)
                resultado+=(numero*contador)
                contador+=1
        if(resultado%11==0):
            print("ISBN Valido")
        else:
            print("ISBN no valido")
    return "Proceso finalizado"

def archivo():
    f=open("./FS2017raw_tlmy_146bytes.bin","rb")
    ssize= 146
    chunk=f.read(ssize)
    temperaturas = list()
    while(chunk):
        chunk = chunk[56:58]
        temperatura=struct.unpack("h",chunk)
        temperatura_celsius=temperatura[0]*0.3899-67.84
        temperaturas.append(temperatura_celsius)
        chunk=f.read(ssize)
    f.close()
    print(temperaturas)    
    x=range(0,len(temperaturas))
    y=temperaturas
    plt.plot(x,y)
    plt.show()
    return "Proceso finalizado"