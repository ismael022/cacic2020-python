'''
Created on 07/10/2020

@author: cacic2020
'''
from Place import Place   
 
class Hotel(Place):
    def __init__(self,*args,**kwargs) :
        self.__valoracion = list()
        self.__simbolo=kwargs["simbolo"]
        self.__limite=kwargs["limite"]
        
    def getSimbolo(self):
        return self
    def addValoracion(self,valor):
        #Agrega valoraciones
        if(0<=valor and valor <=5):
            self.__valoracion.append(valor)
        else:
            raise Exception("Debe ingresar valores entre 0 a 5")
    def getValoraciones(self):
        #Devuelve valoraciones de una lista
        for i in range(0,len(self.__valoracion)):
            print(self.__valoracion[i])
    
    def getValoracion(self):
        #Devuelve valoracion promedio
        if(self.__limite!=0):
            suma=0.0
            for i in range(0,len(self.__valoracion)):
                suma+=self.__valoracion[i]
            return suma/len(self.__valoracion)
        else:
            raise Exception("El Hotel no tiene ninguna valoracion")
    def addValoracionConLimite(self,valor):
        #Agrega elementos a la lista con un limite definido y luego sobreescribe 
        if(0<=valor and valor <=5):
            if(self.__limite==len(self.__valoracion)):
                self.__valoracion.append(valor)
                self.__valoracion.pop(0)
            else:
                self.__valoracion.append(valor)
        else:
            raise Exception("Ha ocurrido un error: Valor fuera de rango (0 a 5)")