'''
Created on 07/10/2020

@author: cacic2020
'''
from math import radians,cos, sin, atan2, sqrt
class Geoposition():
    def __init__(self,*args,**kwargs) :
        #En Pyhton self=this
        #self.__latitude=kwargs["latitude"]
        #self.__longitude=kwargs["longitude"]
        try:
            llat:0.0
            llong=0.0
        
            if(args):
                llat=args[0]
                llong=args[1]
            else:
                llat=kwargs["latitude"]
                llong=kwargs["longitude"]
                
            if llat<-90 or llong>90:
                raise Exception("Lat between 90 and -90")
            
            self.__latitude=llat
            self.__longitude=llong
            
        except KeyError as ex:
            raise Exception("Ha ocurrido un error: ",ex)
    def getlatitude(self):    
        return self.__latitude
    def getlongitude(self):
        return self.__longitude
    def gtimezon(self):
        return round(self.__longitude*12/180)
    def distancia(self,other):
        R=6373.0
        dlon=radians(other.getlongitude())-radians(self.__longitude)
        dlat=radians(other.getlatitude())-radians(self.__latitude)
        a=sin(dlat/2)**2+\
        cos(radians(self.__latitude))*\
        cos(radians(self.getlatitude()))*\
        sin(dlon/2)**2
        c=2*atan2(sqrt(a),sqrt(1-a))
        return R*c
    def __str__(self):
        return "Geoposition lat= "+str(self.__latitude)+", long="+str(self.__longitude)
        
    def render(self):
        return "render en clase Geoposition "    