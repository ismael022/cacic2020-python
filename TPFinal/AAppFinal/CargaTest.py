'''
Created on 09/10/2020

@author: cacic2020
'''
import sys; print('%s %s' % (sys.executable or sys.platform, sys.version))
import os; os.environ['DJANGO_SETTINGS_MODULE'] = 'TPFinal.settings';
import struct

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()


from AAppFinal.models import Dispositivo,Sensor

#Funcion para cargar archivo device1.dat a la base de datos
def cargarSensores1():
    
   
    
    try:
        sensores1=open("/home/cacic2020/eclipse-workspace/TPFinal/device1.dat","rb")
        
        ssize1=struct.calcsize("HHHHh")
        frag1=sensores1.read(ssize1)
        D1=Dispositivo(name="DISPOSITIVO-1")
        D1.save()
        
        
        while(frag1):
            
            valores1=struct.unpack(">HHHHh",frag1)
            v1=valores1[0]
            v2=valores1[1]
            v3=valores1[2]
            v4=valores1[3]
            v5=valores1[4]
            
            
            Sensor(name="PCM_3V3V",unidad="1",valor=v1*0.003938,dispositivo=D1).save()
            Sensor(name="PCM_3V3A",unidad="2",valor=v2*0.005237,dispositivo=D1).save()
            Sensor(name="PCM_5V5V",unidad="1",valor=v3*0.005865,dispositivo=D1).save()
            Sensor(name="PCM_5V5A",unidad="2",valor=v4*0.005237,dispositivo=D1).save()
            Sensor(name="CPU_V",unidad="3",valor=v5/100,dispositivo=D1).save()
            
            
            frag1=sensores1.read(ssize1)
        
        
        
        sensores1.close()
        
        
    except FileNotFoundError:
        
        raise Exception("no se ha encontrado el archivo Device1.dat")
        
        
        
   
#Funcion para cargar archivo device2.dat a la base de datos   
def cargarSensores2():
    
    try:
        
        sensores2=open("/home/cacic2020/eclipse-workspace/TPFinal/device2.dat","rb")
        
        
       
        ssize2=struct.calcsize("hhhhHHHH")
        frag2=sensores2.read(ssize2)
        D2=Dispositivo(name="DISPOSITIVO-2")
        D2.save()
    
         

        
        while(frag2):
            
            valores2 =struct.unpack(">hhhhHHHH",frag2)
            
            v1=valores2[0]
            v2=valores2[1]
            v3=valores2[2]
            v4=valores2[3]
            v5=valores2[4]
            v6=valores2[5]
            v7=valores2[6]
            v8=valores2[7]
            
            Sensor(name="TEMP",unidad="3",valor=v1/100,dispositivo=D2).save()
            Sensor(name="SVX",unidad="",valor=v2/16384,dispositivo=D2).save()
            Sensor(name="SVY",unidad="",valor=v3/16384,dispositivo=D2).save()
            Sensor(name="SVZ",unidad="",valor=v4/16384,dispositivo=D2).save()
            Sensor(name="PCM_3V3V",unidad="1",valor=v5*0.003938,dispositivo=D2).save()
            Sensor(name="PCM_3V3A",unidad="2",valor=v6*0.005237,dispositivo=D2).save()
            Sensor(name="PCM_5V5V",unidad="1",valor=v7*0.005865,dispositivo=D2).save()
            Sensor(name="PCM_5V5A",unidad="2",valor=v8*0.005237,dispositivo=D2).save()
            
            
            frag2=sensores2.read(ssize2)
            
            
            
        sensores2.close()
    
    except FileNotFoundError:
        
        raise Exception("no se ha encontrado el archivo Device2.dat")
        
    
if __name__ == '__main__':
    
    cargarSensores1()
    cargarSensores2()