from django.contrib import admin
from AAppFinal.models import Sensor,Dispositivo



# Register your models here.


class cargarDispositivos(admin.ModelAdmin):
    pass    
    
    
    
class SensorAdmin(admin.ModelAdmin):
    #Esta linea lista los campos a mostrar en la interfaz de Sensore
    list_display=('name','unidad','valor','dispositivo')
    #Esta linea muestros los filtros disponibles en pantalla para los Sensores
    list_filter = ['unidad','dispositivo']
    #Esta linea utiliza el atributo name como campo de busqueda especifica para un Sensor
    search_fields=['name']
    

admin.site.register(Sensor,SensorAdmin)

class DispositivoAdmin(admin.ModelAdmin):
    #Esta linea lista los campos a mostrar en la interfaz de Dispositivo
    list_display=('name',)
    #Esta linea utiliza el atributo name como campo de busqueda especifica para un Dispositivo
    search_fields=['name']

admin.site.register(Dispositivo,DispositivoAdmin)

