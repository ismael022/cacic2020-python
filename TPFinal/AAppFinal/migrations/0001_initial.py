# Generated by Django 3.1 on 2020-10-13 00:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Dispositivo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='sin nombre', max_length=25, unique=True, verbose_name='nombre de Dispositivo')),
            ],
        ),
        migrations.CreateModel(
            name='Sensor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='sin nombre', max_length=25, verbose_name='nombre de Sensor')),
                ('unidad', models.CharField(blank=True, choices=[('1', 'Volts'), ('2', 'Amperes'), ('3', 'Grados Celcius')], default='0', max_length=32)),
                ('valor', models.FloatField(max_length=25, verbose_name='valor')),
                ('dispositivo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Dispositivo', to='AAppFinal.dispositivo')),
            ],
        ),
    ]
