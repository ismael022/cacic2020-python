from django.db import models
from django.db.models.deletion import CASCADE

from django.core.exceptions import ValidationError

# Create your models here.

    
#Lista de unidades
Unidades_Opciones= (('1', 'Volts'),('2', 'Amperes'),('3', 'Grados Celcius'))
 


class Dispositivo(models.Model):
    
    name =models.CharField(verbose_name='nombre de Dispositivo',max_length=25,default="sin nombre",null=False,unique=True)
    #Dispositivo= models.ForeignKey(to=Dispositivo, on_delete=CASCADE,related_name="sensores")
    
    
    #Funcion para convertir nombre de Dispositivo a mayuscula
    def clean(self):
        self.name = self.name.upper()
    
    
    def __str__(self):
        return self.name
    
class Sensor(models.Model):
    
    name =models.CharField(verbose_name='nombre de Sensor',max_length=25,default="sin nombre",null=False)
    #unidad =models.CharField(verbose_name='nombre de unidad',max_length=25,default="sin nombre",null=False)
    unidad =models.CharField(max_length=32,choices=Unidades_Opciones,blank=True,null=False,default='0')
    
    valor =models.FloatField(verbose_name='valor',max_length=25,null=False)
      
    dispositivo=models.ForeignKey(to=Dispositivo, on_delete=CASCADE,related_name="Dispositivo")
    

    #Funcion para controlar el atributo valor de un Sensor segun la unidad 
    def clean(self):
        
        self.name = self.name.upper()
        
        
        if self.unidad== '1' and (self.valor<1 or self.valor >10):
            
            raise ValidationError('Unidad Volts debe tomar valores entre 1 y 10')
        
        if self.unidad== '2' and (self.valor<1 or self.valor >10):
            
            raise ValidationError('Unidad Amper debe tomar valores entre 1 y 10')
        
        if self.unidad== '3' and (self.valor<0 or self.valor >100):
            
            raise ValidationError('Unidad Grados Celcius debe tomar valores entre 0 y 100')
        
           
    
    
    
    
    
    


